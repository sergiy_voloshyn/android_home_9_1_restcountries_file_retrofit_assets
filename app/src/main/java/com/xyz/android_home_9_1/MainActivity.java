package com.xyz.android_home_9_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;

/*1. Используя сервис https://restcountries.eu/ создать приложение "Каталог стран".
Приложение будет схоже с приложением из предыдущего задания, но есть и не мало отличий.
 Приложение должно уметь показывать список частей света (regions) частей частей света (sub regions)
  и, собственно, стран. Каждый список должен быть на отдельном экране (fragment или activity, выбираете вы).
   Должна быть возможность просмотра информации по стране (столица, население, языки и пр. 5-7 пунктов).
   Важно: данные с сервера должны загружаться только один раз - при первом запуске приложения,
    для последующих запусков использовать локальный кэш (данные можно сохранить в internal storage в json формате).
 1.1* Аналогичен предыдущему заданию. Добавить возможность фильтрации результатов (стран)
по названию, языку и столицам. Должно выглядеть так: пользователь в текстовое поле вводит текст,
если этот текст содержится в названии страны или ее столицы - она остается в списке, если нет - исчезает.
 1.2** Добавить иконки для каждой страны https://www.gosquared.com/resources/flag-icons/.
  Для этого использовать фолдер assets, в него поместить архив с иконками (32×32 будет вполне достаточно).
 При первом старте приложения все иконки необходимо скопировать на sd карту, на экране деталей страны добавить
 соответствующую иконку.

*/
public class MainActivity extends AppCompatActivity implements
        RegionElementClickListener, SubregionElementClickListener, CountriesElementClickListener {


    RegionFragment regionsFragment;

    String region;

    SubregionFragment subregionsFragment;

    CountriesFragment countriesFragment;

    CountryInfoFragment countryInfoFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        regionsFragment = new RegionFragment();
        subregionsFragment = new SubregionFragment();
        countriesFragment = new CountriesFragment();
        countryInfoFragment = new CountryInfoFragment();


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, regionsFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onRegionItemClick(String item) {

        subregionsFragment.setSubregion(item);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, subregionsFragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onSubregionItemClick(String item) {

        countriesFragment.setSubregion(item);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, countriesFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCountriesItemClick(String item) {
        countryInfoFragment.setCountry(item);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.base_container, countryInfoFragment)
                .addToBackStack(null)
                .commit();

    }
}
