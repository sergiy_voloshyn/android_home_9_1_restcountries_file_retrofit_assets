package com.xyz.android_home_9_1;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


abstract public class BaseFragment extends Fragment {


    @BindView(R.id.list)
    RecyclerView listView;

    List<String> dataList;
    ListAdapter listAdapter;


    RegionElementClickListener regionElementClickListener;
    SubregionElementClickListener subregionElementClickListener;
    CountriesElementClickListener countriesElementClickListener;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof RegionElementClickListener) {
            regionElementClickListener = (RegionElementClickListener) context;
        } else {
            throw new RuntimeException("Must be MainActivity");
        }

        if (context instanceof SubregionElementClickListener) {
            subregionElementClickListener = (SubregionElementClickListener) context;
        } else {
            throw new RuntimeException("Must be MainActivity");
        }
        if (context instanceof CountriesElementClickListener) {
            countriesElementClickListener = (CountriesElementClickListener) context;
        } else {
            throw new RuntimeException("Must be MainActivity");
        }

    }


    abstract public BaseFragment newInstance();

    abstract public void setListData(List<String> dataList);


}

