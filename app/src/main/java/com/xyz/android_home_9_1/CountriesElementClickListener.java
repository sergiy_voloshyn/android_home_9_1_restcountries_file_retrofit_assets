package com.xyz.android_home_9_1;

/**
 * Created by user on 01.02.2018.
 */

public interface CountriesElementClickListener {

    void onCountriesItemClick(String item);
}
